package main

import (
	"bufio"
	"os"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func get_site_list() []string {
	var sites []string
	file, err := os.Open("ftpsites")
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		sites = append(sites ,scanner.Text())
	}
	check(scanner.Err())
	return sites
}
