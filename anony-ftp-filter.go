package main

import (
	"bufio"
	"fmt"
	ftp4go "ftp4go"
	"os"
	"time"
)

func try_ftp_anonymous(site string) bool {
	var err error
	ftpClient := ftp4go.NewFTP(0)
	_, err = ftpClient.Connect(site, ftp4go.DefaultFtpPort, "")
	if err != nil {
		return false
	}
	defer ftpClient.Quit()
	_, err = ftpClient.Login("anonymous", "", "")
	if err != nil {
		return false
	}
	return true
}

func is_ftp_anonymous(site string) bool {
	ret := make(chan string)
	go func() {
		if try_ftp_anonymous(site) {
			ret <- site
		}
	}()
	select {
	case <-ret:
		return true
	case <-time.After(time.Second * 1):
		return false
	}
	return false
}

func main() {
	start_time := time.Now()
	f_out, err := os.Create("anony-ftp-list")
	check(err)
	defer f_out.Close()
	w := bufio.NewWriter(f_out)
	for _, site := range get_site_list() {
		fmt.Println("Testing site :", site)
		if is_ftp_anonymous(site) {
			_, err = w.WriteString(site + "\n")
			w.Flush()
			fmt.Println("Found Anonymous FTP", site)
		}
	}
	fmt.Printf("run %f", time.Since(start_time).Seconds())
}
