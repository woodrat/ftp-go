package main

import (
	"fmt"
	ftp4go "ftp4go"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	site := "127.0.0.1"
	var err error
	ftpClient := ftp4go.NewFTP(0)
	_, err = ftpClient.Connect(site, ftp4go.DefaultFtpPort, "")
	check(err)
	defer ftpClient.Quit()
	_, err = ftpClient.Login("anonymous", "", "")
	check(err)
	fmt.Println("Anonymous FTP!")
}
